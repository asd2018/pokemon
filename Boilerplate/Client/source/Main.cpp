﻿#include <iostream>
#include <memory>

#include "Session.h"


int main(int argc, char** argv)
{
	const auto host = "127.0.0.1";
	const auto port = "10568";

	// The io_context is required for all I/O
	boost::asio::io_context ioContext;

	std::string task;

	RequestsManager requestsManager;

	task = requestsManager.getMap().at("Task1")->getContentAsString();
	std::make_shared<Session>(ioContext)->run(host, port, task);
	task = requestsManager.getMap().at("Task2")->getContentAsString();
	std::make_shared<Session>(ioContext)->run(host, port, task);
	task = requestsManager.getMap().at("Task3")->getContentAsString();
	std::make_shared<Session>(ioContext)->run(host, port, task);
	task = requestsManager.getMap().at("Task4")->getContentAsString();
	std::make_shared<Session>(ioContext)->run(host, port, task);
	task = requestsManager.getMap().at("Task5")->getContentAsString();
	std::make_shared<Session>(ioContext)->run(host, port, task);

	// Run the I/O service. The call will return when
	// the socket is closed.
	ioContext.run();

	system("pause");
	return EXIT_SUCCESS;

}
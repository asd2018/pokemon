#include "RequestsManager.h"
#include "Task1Request.h"
#include "Task2Request.h"
#include "Task3Request.h"
#include "Task4Request.h"
#include "Task5Request.h"

RequestsManager::RequestsManager()
{
	this->requests.emplace("Task1", std::make_shared<Task1Request>());
	this->requests.emplace("Task2", std::make_shared<Task2Request>());
	this->requests.emplace("Task3", std::make_shared<Task3Request>());
	this->requests.emplace("Task4", std::make_shared<Task4Request>());
	this->requests.emplace("Task5", std::make_shared<Task5Request>());
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}

#include "Task4Request.h"

#include <iostream>

Task4Request::Task4Request() : Request("Task4")
{
	std::string type1, type2;

	std::cout << "Introduceti date pentru cerinta 4 \n \n"
		<< "Tipuri disponibile: Bug, Dark, Dragon, Electric, Fairy, Fighting, Fire, Flying, Ghost, Grass, Ground, Ice, Normal, Poison, Psychic, Rock, Steel, Water \n";

	std::cout << "Scrieti primul tip:";
	std::cin >> type1;

	std::cout << "\nTipuri disponibile: Bug, Dark, Dragon, Electric, Fairy, Fighting, Fire, Flying, Ghost, Grass, Ground, Ice, Normal, Poison, Psychic, Rock, Steel, Water, Nothing \n\n";

	std::cout << "Scrieti al doilea tip:";
	std::cin >> type2;

	if (type2 == "Nothing")
	{
		type2 = "";
	}

	this->content.push_back(boost::property_tree::ptree::value_type("Type1", type1));
	this->content.push_back(boost::property_tree::ptree::value_type("Type2", type2));
}

#include "ResponsesManager.h"
#include "Task1Response.h"
#include "Task2Response.h"
#include "Task3Response.h"
#include "Task4Response.h"
#include "Task5Response.h"

ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("Task1", std::make_shared<Task1Response>());
	this->Responses.emplace("Task2", std::make_shared<Task2Response>());
	this->Responses.emplace("Task3", std::make_shared<Task3Response>());
	this->Responses.emplace("Task4", std::make_shared<Task4Response>());
	this->Responses.emplace("Task5", std::make_shared<Task5Response>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}

#include "Task4Response.h"

#include <fstream>

#include "Pokemon.h"

Task4Response::Task4Response()
{

}

std::string Task4Response::interpretPacket(const boost::property_tree::ptree & packet)
{
	const std::string path = "../../../Pokemon.csv";

	std::ifstream file;
	file.open(path);

	auto type1 = packet.get<std::string>("Type1");
	auto type2 = packet.get<std::string>("Type2");

	std::string lineToDel;
	std::getline(file, lineToDel);

	long total_of_totals = 0;

	std::pair<std::string, std::string> types(type1, type2);

	for (std::string line; std::getline(file, line); )
	{
		Pokemon pokemon(line);
		if (pokemon.getTypes() == types)
			total_of_totals += pokemon.getTotal();
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "task4.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", std::to_string(total_of_totals)));

	return this->getContentAsString();
}

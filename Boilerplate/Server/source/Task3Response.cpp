#include "Task3Response.h"

#include<fstream>

#include "Pokemon.h"

Task3Response::Task3Response()
{

}

std::string Task3Response::interpretPacket(const boost::property_tree::ptree & packet)
{
	const std::string path = "../../../Pokemon.csv";
	double sumHP = 0.0;
	uint16_t numberOfPokemon = 0;

	std::ifstream file;
	file.open(path);

	std::string lineToDel; 
	std::getline(file, lineToDel);

	for (std::string line; std::getline(file, line); )
	{
		Pokemon pokemon(line);
		sumHP += pokemon.getHp();
		++numberOfPokemon;
	}

	double result = sumHP / numberOfPokemon;

	this->content.push_back(boost::property_tree::ptree::value_type("File", "task3.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", std::to_string(result)));

	return this->getContentAsString();
}

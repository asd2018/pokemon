#include "Task1Response.h"

#include "PokemonMap.h"
#include "Pokemon.h"

Task1Response::Task1Response()
{

}

std::string Task1Response::interpretPacket(const boost::property_tree::ptree & packet)
{
	const std::string path = "../../../Pokemon.csv";
	PokemonMap map(path, KeyType::Total);
	auto topPokemon = map.getTopN(10);

	std::string result = "";

	uint8_t index = 0; 
	for (auto& pokemon : topPokemon)
	{
		result.append(std::to_string(index + 1) + ". Name: " + pokemon.getName() + ". Total: " + std::to_string(pokemon.getTotal()) + " ");
		index++;
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "task1.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result : ", result));
	return this->getContentAsString();

}
#include "Task5Response.h"

#include "PokemonMap.h"

Task5Response::Task5Response()
{

}

std::string Task5Response::interpretPacket(const boost::property_tree::ptree & packet)
{
	const std::string path = "../../../Pokemon.csv";
	PokemonMap map(path);

	uint16_t maxAppearances = 0, minAppearances = UINT16_MAX;
	std::string mostCommonType, leastCommonType;
	auto types = map.getAllTypes();
	
	for (auto type : types)
	{
		if (type.first == "")
		{
			continue;
		}
		if (type.second > maxAppearances)
		{
			mostCommonType = type.first;
			maxAppearances = type.second;
		}
		if (type.second < minAppearances)
		{
			leastCommonType = type.first;
			minAppearances = type.second;
		}
	}

	std::string result = "Cel mai des inalnit tip: " + mostCommonType + "(" + std::to_string(maxAppearances) + " ori); " +
		" Cel mai rar tip: " + leastCommonType  + "(" + std::to_string(minAppearances) + " ori)";

	this->content.push_back(boost::property_tree::ptree::value_type("File", "task5.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", result));

	return this->getContentAsString();

}
#include "PokemonMap.h"

#include<fstream>

PokemonMap::PokemonMap(const std::string& path, KeyType primaryKey)
{
	std::ifstream file;
	file.open(path);

	std::string line1; std::getline(file, line1);

	for (std::string line; std::getline(file, line); )
	{
		Pokemon value(line);
		int key2 = 0;

		switch (primaryKey)
		{
		case None: key2 = value.getNumber(); break;
		case Total: key2 = value.getTotal(); break;
		case Speed: key2 = value.getSpeed(); break;
		}

		map.insert(std::pair<Key, Pokemon>(Key(key2, value.getName()), value));
	}

	file.close();
}


PokemonMap::~PokemonMap()
{
	// empty
}

std::vector<Pokemon> PokemonMap::getTopN(int number)
{
	std::vector<Pokemon> pokemonList;
	int count = 0;

	for (const auto& keyValuePair : map)
	{
		count++;
		pokemonList.push_back(keyValuePair.second);
		if (count == number)
		{
			break;
		}	
	}

	return pokemonList;
}

std::unordered_map<std::string, uint16_t> PokemonMap::getAllTypes()
{
	std::unordered_map<std::string, uint16_t> types;

	for (auto& keyValuePair : map)
	{
		std::pair<std::string, std::string> pair = keyValuePair.second.getTypes();
		types[pair.first]++;
		types[pair.second]++;
	}

	return types;
}

std::map<Key, Pokemon, std::greater<Key>> PokemonMap::getMap()
{
	return map;
}

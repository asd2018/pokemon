#include "Task2Response.h"

#include "PokemonMap.h"
#include "Pokemon.h"

Task2Response::Task2Response()
{

}

std::string Task2Response::interpretPacket(const boost::property_tree::ptree & packet)
{
	const std::string path = "../../../Pokemon.csv";
	PokemonMap map(path, KeyType::Speed);
	auto topPokemon = map.getTopN(10);

	std::string result = "";

	uint8_t index = 0;
	for (auto& pokemon : topPokemon)
	{
		result.append(std::to_string(index + 1) + ". Name: " + pokemon.getName() + ". Speed: " + std::to_string(pokemon.getSpeed()) + " ");
		index++;
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "task2.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result ", result));
	return this->getContentAsString();

}
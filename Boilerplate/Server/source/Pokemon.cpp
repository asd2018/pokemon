#include "Pokemon.h";

#include<boost/tokenizer.hpp>

Pokemon::Pokemon()
{
	// empty
}

Pokemon::Pokemon(uint16_t number, std::string name, std::string type1, std::string type2, uint16_t total, uint8_t hp, uint8_t attack, uint8_t defense, uint8_t spAtk, uint8_t spDef, uint8_t speed) :
	number(number),
	name(name),
	type1(type1),
	type2(type2),
	total(total),
	hp(hp),
	attack(attack),
	defense(defense),
	spAtk(spAtk),
	spDef(spDef),
	speed(speed)
{
	// empty
}

Pokemon::Pokemon(const std::string& line)
{
	boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
	boost::tokenizer<boost::char_separator<char> > tokens(line, sep);
	boost::tokenizer<boost::char_separator<char> >::iterator it = tokens.begin();

	for (int index = 0; it != tokens.end(); ++it, ++index)
	{
		switch (index)
		{
		case 0: number = atoi((*it).c_str()); break;
		case 1: name = (*it).c_str(); break;
		case 2: type1 = (*it).c_str(); break;
		case 3: type2 = (*it).c_str(); break;
		case 4: total = atoi((*it).c_str()); break;
		case 5: hp = atoi((*it).c_str()); break;
		case 6: attack = atoi((*it).c_str()); break;
		case 7: defense = atoi((*it).c_str()); break;
		case 8: spAtk = atoi((*it).c_str()); break;
		case 9: spDef = atoi((*it).c_str()); break;
		case 10: speed = atoi((*it).c_str()); break;
		}
	}
}

Pokemon::~Pokemon()
{
	// empty
}

uint16_t Pokemon::getNumber()
{
	return number;
}

uint16_t Pokemon::getTotal()
{
	return total;
}

uint8_t Pokemon::getHp()
{
	return hp;
}

uint8_t Pokemon::getAttack()
{
	return attack;
}

uint8_t Pokemon::getDefense()
{
	return defense;
}

uint8_t Pokemon::getSpecialAttack()
{
	return spAtk;
}

uint8_t Pokemon::getSpecialDefense()
{
	return spDef;
}

uint8_t Pokemon::getSpeed()
{
	return speed;
}

std::pair<std::string, std::string> Pokemon::getTypes()
{
	return std::pair<std::string, std::string>(type1, type2);
}

std::string Pokemon::getName()
{
	return name;
}

std::string Pokemon::toString()
{
	std::string temp;
	int minSize = 0;
	const int nameSize = 30, numberSize = 6, typeSize = 15;

	temp += std::to_string(number);
	minSize += numberSize;
	temp.append(minSize - temp.length(), ' ');

	temp += name;
	minSize += nameSize;
	temp.append(minSize - temp.length(), ' ');

	temp += type1;
	minSize += typeSize;
	temp.append(minSize - temp.length(), ' ');

	temp += type2;
	minSize += typeSize;
	temp.append(minSize - temp.length(), ' ');

	temp += std::to_string(total);
	minSize += numberSize;
	temp.append(minSize - temp.length(), ' ');

	temp += std::to_string(hp);
	minSize += numberSize;
	temp.append(minSize - temp.length(), ' ');

	temp += std::to_string(attack);
	minSize += numberSize;
	temp.append(minSize - temp.length(), ' ');

	temp += std::to_string(defense);
	minSize += numberSize;
	temp.append(minSize - temp.length(), ' ');

	temp += std::to_string(spAtk);
	minSize += numberSize;
	temp.append(minSize - temp.length(), ' ');

	temp += std::to_string(spDef);
	minSize += numberSize;
	temp.append(minSize - temp.length(), ' ');

	temp += std::to_string(speed);
	minSize += numberSize;
	temp.append(minSize - temp.length(), ' ');

	return temp;
}
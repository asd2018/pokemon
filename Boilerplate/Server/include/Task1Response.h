#pragma once

#include "Response.h"

class Task1Response : public Framework::Response
{
public:
	Task1Response();

	std::string interpretPacket(const boost::property_tree::ptree& packet);

};
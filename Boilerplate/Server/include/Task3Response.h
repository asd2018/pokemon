#pragma once

#include "Response.h"

class Task3Response : public Framework::Response
{
public:
	Task3Response();

	std::string interpretPacket(const boost::property_tree::ptree& packet);

};
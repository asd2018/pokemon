#pragma once

#include "Response.h"

class Task5Response : public Framework::Response
{
public:
	Task5Response();

	std::string interpretPacket(const boost::property_tree::ptree& packet);

};
#pragma once

#include "Response.h"

class Task4Response : public Framework::Response
{
public:
	Task4Response();

	std::string interpretPacket(const boost::property_tree::ptree& packet);

};
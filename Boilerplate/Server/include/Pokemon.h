#pragma once

#include<string>

class Pokemon
{
public:
	Pokemon();
	Pokemon(uint16_t number, std::string name, std::string type1, std::string type2, uint16_t total, uint8_t hp, uint8_t attack, uint8_t defense, uint8_t spAtk, uint8_t spDef, uint8_t speed);
	
	// Accepts strings of the following format:
	//#,Name,Type 1,Type 2,Total,HP,Attack,Defense,Sp. Atk,Sp. Def,Speed
	Pokemon(const std::string& line);

	~Pokemon();

	uint16_t getNumber();
	uint16_t getTotal();
	uint8_t getHp();
	uint8_t getAttack();
	uint8_t getDefense();
	uint8_t getSpecialAttack();
	uint8_t getSpecialDefense();
	uint8_t getSpeed();
	std::pair<std::string, std::string> getTypes();
	std::string getName();

	// Returns a string which contains all of the Pokemon's atributes in the following format:
	//#, Name, Type 1, Type 2, Total, HP, Attack, Defense, Sp. Atk, Sp. Def, Speed
	std::string toString();

private:
	uint16_t number, total;
	uint8_t hp, attack, defense, spAtk, spDef, speed;
	std::string type1, type2, name;
};


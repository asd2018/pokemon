#pragma once

#include "Response.h"

class Task2Response : public Framework::Response
{
public:
	Task2Response();

	std::string interpretPacket(const boost::property_tree::ptree& packet);

};
#pragma once

#include <map>
#include <unordered_map>

#include "Pokemon.h"

typedef std::pair<int, std::string> Key;

enum KeyType : uint8_t
{
	None,
	Total,
	Speed
};

class PokemonMap
{
public:
	// path = path to a file containing the Pokemon list
	// primaryKey = the attribute by which the map will be ordered
	PokemonMap(const std::string& path, KeyType primaryKey = None);

	~PokemonMap();

	// Retrieves a list containing the top N Pokemon
	std::vector<Pokemon> getTopN(int number);

	// Retrieves an unordered map containing all types and the amount of times they are found
	std::unordered_map<std::string, uint16_t> getAllTypes();

	// Retrieves the base map
	std::map<Key, Pokemon, std::greater<Key>> getMap();
private:
	std::map<Key, Pokemon, std::greater<Key>> map;
};


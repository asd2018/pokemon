Plan
	
Proiect propus: PROIECT 9: POKEMON

Echipa:
	Cărăuș Denis-Victor (10LF271)
	Buzilă Florina Alexandra (10LF271)
	Cosma Ștefan (10LF271)

Probleme:

	Stabilite:
		1. 	Top 10 Pokemoni după puterea lor totala
		2. 	Top 10 cei mai rapizi Pokemoni 
		3. 	Media HP-ului tuturor Pokemonilor
		4. 	Precizat tipul1 și tipul2 sa facem totalul de totale
		5. 	Cel mai des întâlnit și cel mai rar tip
		
	Alternative(de discutat la curs/laborator daca nu ne iese una din problemele stabilite):
		1. 	Lista Pokemonilor cu un singur tip
				Sau procentul de Pokemoni cu un tip, cu doua tipuri și diferența
		2. 	Numărul de pokemoni care au Attack-ul mai mare decât Defense-ul 
				Posibil: + Special Attack și Special Defense
		3. 	Lista cu Pokemoni în care vor apărea cei cu: cel mai mare atac, cea mai mare defensiva, cea mai mare viata, cel mai mare atac special, cea mai mare defensiva speciala, cea mai mare putere și cea mai mare viteaza dintr-un tip ales de utilizator.

Framework: 
	Cel de la laborator.

Link Repo:
	https://bitbucket.org/asd2018/pokemon/src




